# base

## Install miniconda

Download and install Miniconda (we like to use the `~/conda` install folder).

## Install additional packages

```bash
conda install -y -q \
    byobu xsv ripgrep \
    nodejs jupyterlab jupyterlab_code_formatter \
    isort black
jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter labextension install @jupyterlab/toc
jupyter labextension install @ryantam626/jupyterlab_code_formatter
jupyter serverextension enable --py jupyterlab_code_formatter
```

## Add custom settings

Find the folder where jupyter stores its settings files.

```bash
JUPYTER_CONFIG_DIR="$(jupyter --config-dir)"
echo "${JUPYTER_CONFIG_DIR}"  # /home/kimlab1/{username}/.jupyter
```

### shortcuts-extension

```bash
KEYBOARD_SHORTCUT_SETTINGS="${JUPYTER_CONFIG_DIR}/lab/user-settings/@jupyterlab/shortcuts-extension/shortcuts.jupyterlab-settings"
cat <<'EOF' > "${KEYBOARD_SHORTCUT_SETTINGS}"
{
    "shortcuts":
    [
        {
            "command": "jupyterlab_code_formatter:black",
            "keys": [
                "Ctrl Shift I",
            ],
            "selector": ".jp-Notebook.jp-mod-editMode"
        },
            {
            "command": "jupyterlab_code_formatter:isort",
            "keys": [
                "Ctrl Shift O",
            ],
            "selector": ".jp-Notebook.jp-mod-editMode"
        },
        {
            "command": "jupyterlab_code_formatter:isort",
            "keys": [
                "Ctrl Shift U",
            ],
            "selector": ".jp-Notebook.jp-mod-editMode"
        },
        {
            "command": "documentsearch:start",
            "keys": [
                "Accel F"
            ],
            "selector": ".jp-mod-searchable",
            "disabled": true
        },
        {
            "command": "documentsearch:start",
            "keys": [
                "Ctrl Shift F"
            ],
            "selector": ".jp-mod-searchable"
        }
    ]
}
EOF
cat "${KEYBOARD_SHORTCUT_SETTINGS}"
```

### jupyterlab_code_formatter

```bash
CODE_FORMATTER_SETTINGS="${JUPYTER_CONFIG_DIR}/lab/user-settings/@ryantam626/jupyterlab_code_formatter/settings.jupyterlab-settings"
cat <<EOF > "${CODE_FORMATTER_SETTINGS}"
{
     "black": {
        "line_length": 100,
        "string_normalization": true
    },
    "isort": {
        "multi_line_output": 3,
        "include_trailing_comma": true,
        "force_grid_wrap": 0,
        'use_parentheses': true,
        "line_length": 100,
        "default_section": "THIRDPARTY"
    }
}
EOF
cat "${CODE_FORMATTER_SETTINGS}"
```

## Add additional environments

In the very least, add the `default` environment.

```bash
wget http://conda-envs.proteinsolver.org/default/default-v30.tar.gz
mkdir -p ~/conda/envs/default
tar -xzf default-v30.tar.gz -C ~/conda/envs/default
conda info --envs  # You should now have the default environment available
```

## Scripted version

```bash
ROOT_DIR="/data"
CONDA_ENV_VERSION="v31"

sudo apt-get -yq update
sudo apt-get -yq install git
git clone https://gitlab.com/conda-envs/base.git

cd base
sudo ./scripts/00_gcp_startup_script.sh "${ROOT_DIR}"
./scripts/01_conda_install.sh "${ROOT_DIR}"
./scripts/02_conda_base_setup.sh
./scripts/03_conda_env_setup.sh "${ROOT_DIR}" "${CONDA_ENV_VERSION}"

cd "${ROOT_DIR}"
mkdir -p adjacency-net-v2/v0.3/pdb-core/
gsutil -m rsync -r gs://speedtest-us/pdb-analysis/pdb-core/ adjacency-net-v2/v0.3/pdb-core/

cd
ln -s "${ROOT_DIR}" datapkg-data-dir
```
