#!/bin/bash

set -ev

ROOT_DIR="$1"
DEFAULT_ENV="$2"

# Conda functions are not exported by default inside bash scripts
# See: https://github.com/conda/conda/issues/7980
source "${ROOT_DIR}/conda/etc/profile.d/conda.sh"

# Install the default conda environment
rm -rf "${ROOT_DIR}/conda/envs/default"
mkdir -p "${ROOT_DIR}/conda/envs/default"
if [[ ! -f default.tar.gz ]] ; then
    curl -s -L http://conda-envs.proteinsolver.org/default/default-${DEFAULT_ENV}.tar.gz > default.tar.gz
fi
tar -xzf default.tar.gz -C "${ROOT_DIR}/conda/envs/default"

conda activate "${ROOT_DIR}/conda/envs/default"
conda-unpack
ipython kernel install --user --name python3
conda activate base

# Install settings
JUPYTER_KERNEL_DIR="$(jupyter kernelspec list --json | jq -r .kernelspecs.python3.resource_dir)"
mkdir -p "${JUPYTER_KERNEL_DIR}"

KERNEL_SETTINGS="${JUPYTER_KERNEL_DIR}/kernel.json"

cat <<EOF > "${KERNEL_SETTINGS}"
{
 "argv": [
  "${ROOT_DIR}/conda/envs/default/bin/conda_ipykernel_launcher.sh",
  "{connection_file}"
 ],
 "display_name": "Python 3",
 "language": "python"
}
EOF

jupyter kernelspec list
