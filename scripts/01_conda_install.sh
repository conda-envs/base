#!/bin/bash

set -ev

ROOT_DIR="$1"

rm -rf "${ROOT_DIR}/conda"
curl -s -L https://repo.anaconda.com/miniconda/Miniconda3-py38_4.8.2-Linux-x86_64.sh > miniconda.sh
openssl dgst -sha256 miniconda.sh | grep 5bbb193fd201ebe25f4aeb3c58ba83feced6a25982ef4afa86d5506c3656c142
bash miniconda.sh -b -p "${ROOT_DIR}/conda"
${ROOT_DIR}/conda/condabin/conda init
