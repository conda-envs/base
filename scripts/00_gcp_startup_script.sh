#!/bin/bash

set -ev

ROOT_DIR="$1"

# Mount file system
sudo apt-get update
sudo apt-get install mdadm --no-install-recommends
sudo mdadm --create /dev/md0 --level=0 --raid-devices=4 \
    /dev/nvme0n1 /dev/nvme0n2 /dev/nvme0n3 /dev/nvme0n4
sudo mkfs.ext4 -F /dev/md0
sudo mkdir -p "${ROOT_DIR}"
sudo mount /dev/md0 "${ROOT_DIR}"
sudo chmod a+w "${ROOT_DIR}"
