#!/bin/bash

set -ev

# Update conda settings
cat <<EOF > ~/.condarc
channel_priority: true
channels:
- pytorch
- conda-forge
- defaults
- kimlab
- ostrokach-forge
- bioconda
- salilab
- omnia
EOF

# Install root packages
conda install -y -q \
    byobu git xsv ripgrep jq \
    nodejs jupyterlab jupyterlab_code_formatter \
    isort black
jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter labextension install @jupyterlab/toc
jupyter labextension install @ryantam626/jupyterlab_code_formatter
jupyter serverextension enable --py jupyterlab_code_formatter

# Install settings
JUPYTER_CONFIG_DIR="$(jupyter --config-dir)"

KEYBOARD_SHORTCUT_SETTINGS="${JUPYTER_CONFIG_DIR}/lab/user-settings/@jupyterlab/shortcuts-extension/shortcuts.jupyterlab-settings"
mkdir -p $(dirname "${KEYBOARD_SHORTCUT_SETTINGS}")
cat <<'EOF' > "${KEYBOARD_SHORTCUT_SETTINGS}"
{
    "shortcuts":
    [
        {
            "command": "jupyterlab_code_formatter:black",
            "keys": [
                "Ctrl Shift I",
            ],
            "selector": ".jp-Notebook.jp-mod-editMode"
        },
            {
            "command": "jupyterlab_code_formatter:isort",
            "keys": [
                "Ctrl Shift O",
            ],
            "selector": ".jp-Notebook.jp-mod-editMode"
        },
        {
            "command": "jupyterlab_code_formatter:isort",
            "keys": [
                "Ctrl Shift U",
            ],
            "selector": ".jp-Notebook.jp-mod-editMode"
        },
        {
            "command": "documentsearch:start",
            "keys": [
                "Accel F"
            ],
            "selector": ".jp-mod-searchable",
            "disabled": true
        },
        {
            "command": "documentsearch:start",
            "keys": [
                "Ctrl Shift F"
            ],
            "selector": ".jp-mod-searchable"
        }
    ]
}
EOF

CODE_FORMATTER_SETTINGS="${JUPYTER_CONFIG_DIR}/lab/user-settings/@ryantam626/jupyterlab_code_formatter/settings.jupyterlab-settings"
mkdir -p $(dirname "${CODE_FORMATTER_SETTINGS}")
cat <<EOF > "${CODE_FORMATTER_SETTINGS}"
{
     "black": {
        "line_length": 100,
        "string_normalization": true
    },
    "isort": {
        "multi_line_output": 3,
        "include_trailing_comma": true,
        "force_grid_wrap": 0,
        'use_parentheses': true,
        "line_length": 100,
        "default_section": "THIRDPARTY"
    }
}
EOF
